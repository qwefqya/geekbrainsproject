using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LearnProject
{
    public class finish : MonoBehaviour
    {

        [SerializeField] private int _lastscene;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log("finish");
                if (SceneManager.GetActiveScene().buildIndex == _lastscene)
                { SceneManager.LoadScene(0); }
                    
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }

        }
    }
}