using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Events;

namespace LearnProject
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] public Button _Quit;
        [SerializeField] public Button _Start;
        [SerializeField] public Button _Sound;
        [SerializeField] public GameObject _SoundMenu;
     

        private void Awake()
        {
            _Start.onClick.AddListener(Play);
            _Quit.onClick.AddListener(Quit);
            _Sound.onClick.AddListener(Sound);
   
        }

        private void Play()
        {
            SceneManager.LoadScene(1);
            Time.timeScale = 1;
        }

        private void Quit()
        {
            Application.Quit();

        }

        private void Sound()
        {
            
            _SoundMenu.SetActive(true); 
         

        }
    }
}