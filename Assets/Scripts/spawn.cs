using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LearnProject
{

    public class spawn : MonoBehaviour
    {


        [SerializeField] public GameObject EnemyPrefab;
        [SerializeField] public Transform SpawnPosition;


        // Start is called before the first frame update
        void Start()
        {
            SpawnEnemy();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void SpawnEnemy()
        {
            var enemy = Instantiate(EnemyPrefab, SpawnPosition.position, SpawnPosition.rotation);


        }
    }
}