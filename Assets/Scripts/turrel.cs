using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LearnProject
{
    public class turrel : MonoBehaviour
    {

        [SerializeField] private Player _player;
        [SerializeField] private GameObject _bulletPrefab;
        [SerializeField] private Transform _spawnPosition;
        [SerializeField] private bool _isFire;
        [SerializeField] private float _cooldown;
        [SerializeField] private float _bulletspeed;


        void Start()
        {
            _player = FindObjectOfType<Player>();
        }


        void Update()
        {
            
            //Debug.DrawRay(_spawnPosition.position, -(_player.transform.position - _spawnPosition.position).normalized, Color.red);
           // Debug.DrawRay(_player.transform.position, (_spawnPosition.position - _player.transform.position).normalized, Color.blue);
            transform.rotation = Quaternion.LookRotation(_player.transform.position - transform.position);
            Ray ray = new Ray(_spawnPosition.position, transform.forward);
           


            if (Physics.Raycast(ray, out RaycastHit hit, maxDistance:4 ))
               if (hit.collider.CompareTag("Player"))
            if (Vector3.Distance(transform.position, _player.transform.position) < 4)
            {
                        if (_isFire)
                Fire();
            }

       
        }

        private void Fire()
        {
            _isFire = false;
            var bulletObj = Instantiate(_bulletPrefab, _spawnPosition.position, _spawnPosition.rotation);
            var bullet = bulletObj.GetComponent<bullet>();
            bullet.Init(_player.transform, 1, _bulletspeed);

            Invoke(nameof(Reloading), _cooldown);
        }

        private void Reloading()
        {
            _isFire = true;
        }
    }
}