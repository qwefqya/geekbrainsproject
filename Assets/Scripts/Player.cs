using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace LearnProject
{

    public class Player : MonoBehaviour
    {
        // Start is called before the first frame update

        [SerializeField] private Vector3 _direction;
        [SerializeField] public float _speed = 4f;
        [SerializeField] public float _speedrotate = 40f;
        [SerializeField] public float _jumpforce = 3f;
        [SerializeField] public float _grenadeforce = 35f;
        [SerializeField] public Rigidbody _rb;
        [SerializeField] public GameObject _grenade;
        [SerializeField] public GameObject _mine;
        [SerializeField] public GameObject _launch;
        [SerializeField] public GameObject _camera;
        [SerializeField] private bool _isFire;
        [SerializeField] private float _cooldownfire;
        [SerializeField] private float _cooldownjump;
        [SerializeField] private bool _isJumping;
        [SerializeField] public int _minecount;
        [SerializeField] private UnityEvent<int> ChangeMineCount;



        // Update is called once per frame

        private void Awake()
        {

            _rb = GetComponent<Rigidbody>();
           


        }


        void Update()
        {
            _direction.x = Input.GetAxis("Horizontal");
            _direction.z = Input.GetAxis("Vertical");

            _direction = transform.TransformDirection(_direction);
            _rb.MovePosition(transform.position + _direction.normalized * _speed * Time.fixedDeltaTime);

            Vector3 rotate = new Vector3(0f, Input.GetAxis("Mouse X") * _speedrotate * Time.fixedDeltaTime, 0f);
            _rb.MoveRotation(_rb.rotation * Quaternion.Euler(rotate));

            //transform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * _speedrotate * Time.fixedDeltaTime);


        }

        private void FixedUpdate()
        {
            //Move(Time.fixedDeltaTime);
            if (_isJumping)
            {
                if (Input.GetKey(KeyCode.Space))
                {
                    _isJumping = false;
                    _rb.AddForce(Vector3.up * _jumpforce);
                    Invoke(nameof(Rejumping), _cooldownjump);

                    // var myVector = new Vector3(0.0f, 1.0f, 0.0f);
                    //transform.position += myVector; //* Time.fixedDeltaTime;
                    //WaitForSeconds(5);
                }
            }

            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (_isFire)
                GrenadeLaunch();
          

                // var myVector = new Vector3(0.0f, 1.0f, 0.0f);
                //transform.position += myVector; //* Time.fixedDeltaTime;
                //WaitForSeconds(5);

            }

            if (Input.GetKey(KeyCode.E))
            {
                if (_isFire)
                {
                    if (_minecount > 0)
                    {
                        MineLaunch();
                        _minecount--;
                        ChangeMineCount.Invoke(_minecount);
                    }
                }

            }

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Ammo"))
            {
                _minecount++;
                ChangeMineCount.Invoke(_minecount);

                Destroy(other.gameObject);
            }

        }



        private void Move(float delta)
        {
            transform.position += _direction * _speed * delta;
        }

        private void GrenadeLaunch()
        {
            _isFire = false;
            GameObject _g = Instantiate(_grenade, _launch.transform.position, _launch.transform.rotation);
            Rigidbody _gp = _g.GetComponent<Rigidbody>();

           // var _grenadeRB = _grenade.GetComponent<Rigidbody>();
            Vector3 _launchdrection = _launch.transform.position - _camera.transform.position;
            // _grenadeRB.AddForce((_launchdrection   ) * _grenadeforce);
            //p.velocity = _launchdrection * _grenadeforce;
            _gp.velocity = (_launchdrection.normalized + Vector3.up/2) * _grenadeforce;
            Invoke(nameof(Reloading), _cooldownfire);


        }

        private void MineLaunch()
        {
            _isFire = false;
            GameObject _g = Instantiate(_mine, _launch.transform.position, _launch.transform.rotation);
       
            Invoke(nameof(Reloading), _cooldownfire);



        }


        private void Reloading()
        {
            _isFire = true;
        }

        private void Rejumping()
        {
            _isJumping = true;
        }

    }
}