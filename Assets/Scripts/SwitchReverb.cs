using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LearnProject
{
    public class SwitchReverb : MonoBehaviour
    {

        [SerializeField] public GameObject _ReverbZone;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _ReverbZone.SetActive(true);
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _ReverbZone.SetActive(false);
            }

        }
    }
}