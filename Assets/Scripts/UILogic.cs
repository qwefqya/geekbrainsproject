using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Events;

namespace LearnProject
{
    public class UILogic : MonoBehaviour
    {
   
       // private bool _Ispause = false;
        //private bool _iscooldown = true;
        [SerializeField] public GameObject _Menu;
        [SerializeField] public GameObject _Interface;
        [SerializeField] public Button _Continiue;
        [SerializeField] public Button _MainMenu;
        [SerializeField] public Button _Restart;
        [SerializeField] private Player _player;
        [SerializeField] public TextMeshProUGUI _minecount;
       // [SerializeField] private UnityEvent _changemine;



        private void Awake()
        {
            _Continiue.onClick.AddListener(Continue);
            _MainMenu.onClick.AddListener(MainMenu);
            _Restart.onClick.AddListener(Restart);
            _minecount.text = "0";
        }

        private void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex );
            Time.timeScale = 1;
        }

        private void MainMenu()
        {
            { SceneManager.LoadScene(0); }
        }

        private void Continue()
        {
            Time.timeScale = 1;
            _Menu.SetActive(false);
            _Interface.SetActive(true);
        }


        // Update is called once per frame
        void Update()
        {

            if (Input.GetKey(KeyCode.Escape))    
                {
                    Time.timeScale = 0;
                 

                    _Menu.SetActive(true);
                    _Interface.SetActive(false);
                }

        }

        public void setammo()
           
        {
            changeminecount(_player._minecount);
        }
        public void changeminecount(int _count)
        { _minecount.text = _count.ToString(); }

    }
}


