using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



namespace LearnProject
{


    public class EnemyMove : MonoBehaviour
    {
        


        [SerializeField] private Player _player;
        [SerializeField] private float _speed = 0.2f;
        private Vector3 _direction;
        private Vector3 _target;

       // private NavMeshAgent _agent;


        void Awake()
        {
            _player = FindObjectOfType<Player>();
            _target = _player.transform.position;
             _direction = -(transform.position - _target);
            //_agent = GetComponent<NavMeshAgent>();

        }

        private void Start()
        {
           // _agent.SetDestination(_player.transform.position);
            

        }


        private void FixedUpdate()
        {
            _target = _player.transform.position;
            _direction = -(transform.position - _target);
            Move(Time.fixedDeltaTime);
           
        }

       

        private void Move(float delta)
        {
            transform.position += _direction * _speed * delta;
        }

    }
}