using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LearnProject
{

    public class bullet : MonoBehaviour
    {
        private Transform _target;
        private float _speed;
        private Rigidbody _rb;

        private void Awake()
        {

            _rb = GetComponent<Rigidbody>();

        }

        public void Init(Transform target, float lifetime, float speed)
        {
            _target = target;
            _speed = speed;
            Destroy(gameObject, lifetime);

        }


        public void Start()
        {
            _rb.velocity = -(transform.position - _target.position) * _speed;
            // transform.position = Vector3.MoveTowards(transform.position, _target.position, _speed);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
           // transform.position = Vector3.MoveTowards(transform.position, _target.position, _speed);
           // _rb.velocity = -(transform.position- _target.position) * _speed;
        }
    }
}

