using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LearnProject
{
    public class trap : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                Destroy(other);

            }
        }
    }
}
