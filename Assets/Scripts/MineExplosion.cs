using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace LearnProject
{
    public class MineExplosion : MonoBehaviour
    {
        [SerializeField] private bool _RocketJump;
        [SerializeField] GameObject _Sound;
        [SerializeField] GameObject _particle;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                Explosion(transform.position, 20f);
                _Sound.SetActive(true);
                _particle.SetActive(true);
                Destroy(gameObject, 1.0f);
            }

        }

        void Explosion(Vector3 center, float radius)
        {
            Collider[] hitColliders = Physics.OverlapSphere(center, radius);
            foreach (var hitCollider in hitColliders)
            {
                if (hitCollider.CompareTag("Enemy"))

                {
                    hitCollider.attachedRigidbody.AddForce(-(center - hitCollider.transform.position)*1000);
                }

               // if (hitCollider.CompareTag("Player"))
               //     if (_RocketJump = true)

              //  {
               //     hitCollider.attachedRigidbody.AddForce(-(center - hitCollider.transform.position) * 1000);
               // }
            }
                
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

