using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace LearnProject
{
    public class AudioScript : MonoBehaviour
    {

          public AudioMixer masterMixer;

        public void SetSoundMusic(float soundLevel)
        {
            masterMixer.SetFloat("MusicVol", soundLevel);
        }

        public void SetSoundFX(float soundLevel)
        {
            masterMixer.SetFloat("FXVol", soundLevel);
        }

    }
}