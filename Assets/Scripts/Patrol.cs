using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace LearnProject
{
    public class Patrol : MonoBehaviour
    {
        [SerializeField] private Player _player;
      

        private Vector3 _direction;
        private Vector3 _target;
        private bool patrol;


        public NavMeshAgent _agent;
        public Transform[] waypoints;

        int _curindex;

        void Awake()
        {
            
            _agent = GetComponent<NavMeshAgent>();
            _player = FindObjectOfType<Player>();

        }

        void Start()
        {
            
            _agent.SetDestination(waypoints[0].position);
            
        }

        // Update is called once per frame

        IEnumerator Stay()
        {
            _agent.SetDestination(transform.position);
            yield return new WaitForSeconds(0.9f);


        }

        void Update()
        {

            _target = _player.transform.position;
           // CheckPlayer();

            patrol = true;

            Ray ray = new Ray(transform.position, transform.forward);



            if (Physics.Raycast(ray, out RaycastHit hit, maxDistance: 15))
            {
                if (hit.collider.CompareTag("Player"))
                    if (Vector3.Distance(transform.position, _player.transform.position) < 15)
                    { patrol = false; }
            }
           

            if (patrol)
            {
                if (_agent.remainingDistance <= _agent.stoppingDistance)
                {
                    _curindex = (_curindex + 1) % waypoints.Length;
                    _agent.SetDestination(waypoints[_curindex].position);
                }
            }
            else
            {
                _agent.SetDestination(_player.transform.position);

            }

        }

        // private void CheckPlayer()
      //  {
            //if ((transform.position - _target).magnitude >= _visionradius) patrol = true;


          //  Ray ray = new Ray(transform.position, transform.forward);



         //   if (Physics.Raycast(ray, out RaycastHit hit, maxDistance: 2))
        //        if (hit.collider.CompareTag("Player"))
        //            if (Vector3.Distance(transform.position, _player.transform.position) < 2)
        //            { patrol = false; }
        //    else { patrol = true; }

           


        //}
    }
}