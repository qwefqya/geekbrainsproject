using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace LearnProject
{
    public class CustomRange : MonoBehaviour
    {

        [SerializeField] public  Slider _slider;

        void Start()
        {
            _slider.minValue = -80;
            _slider.maxValue = 0;
        }
    }
}
