using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace LearnProject
{
    public class ghost : MonoBehaviour
    {


        [SerializeField] private Player _player;
        [SerializeField] private Animator _anim;





        void Awake()
        {
            _player = FindObjectOfType<Player>();
            _anim = GetComponent<Animator>();
        }


        void Update()
        {
            if (Vector3.Distance(transform.position, _player.transform.position) > 4)
                _anim.SetBool("CheckPlayer", false);

            transform.rotation = Quaternion.LookRotation(_player.transform.position - transform.position);
            Ray ray = new Ray(transform.position, transform.forward);



            if (Physics.Raycast(ray, out RaycastHit hit, maxDistance: 4))
                if (hit.collider.CompareTag("Player"))
                    if (Vector3.Distance(transform.position, _player.transform.position) < 4)
                        _anim.SetBool("CheckPlayer", true);


                    

        }
    }
}