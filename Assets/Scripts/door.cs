using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace LearnProject
{


    public class door : MonoBehaviour
    {

        [SerializeField] private Transform _rotatePoint;


        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //  _rotatePoint.rotation = Quaternion.LookRotation(transform.position + Vector3.left * 3);
                _rotatePoint.Rotate(Vector3.up, 60);
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //  _rotatePoint.rotation = Quaternion.LookRotation(transform.position + Vector3.left * 3);
                _rotatePoint.Rotate(Vector3.up, -60);
            }

        }

    }
}